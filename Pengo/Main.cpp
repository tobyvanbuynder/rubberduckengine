#include "MiniginPCH.h"

#include "RubberDuckEngine.h"
#include "Renderer.h"
#include "Pengo.h"

// ReSharper disable once CppUnusedIncludeDirective
#pragma warning( push )
#pragma warning( disable : 4100 )
int main(int argc, char* argv[]) {
#pragma warning( pop )

	rde::WindowSettings windowSettings;
	windowSettings.WindowName = "Programming 4 assignment";
	windowSettings.XPosition = SDL_WINDOWPOS_CENTERED;
	windowSettings.YPosition = SDL_WINDOWPOS_CENTERED;
	windowSettings.Width = 640;
	windowSettings.Height = 480;
	windowSettings.SDLWindowFlags = SDL_WINDOW_SHOWN;

	rde::RenderSettings renderSettings;
	renderSettings.Index = -1;
	renderSettings.SDLRenderFlags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;

	// Init engine
	rde::RubberDuckEngine engine{};
	engine.Initialize(windowSettings, renderSettings);

	// Create game
	Pengo game{};
	engine.SetGame(&game);

	// Run engine
	engine.Run();
	
	// Cleanup
	engine.Cleanup();
	
    return 0;
}