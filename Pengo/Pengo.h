#pragma once

#include "Game.h"

#include "LevelManager.h"

class Pengo final : public rde::Game
{
public:
	explicit Pengo();
	virtual ~Pengo() = default;

	virtual void Initialize() override;

private:
	LevelManager m_LevelManager;

	// Disable copy & move constructor/assignment operator
	Pengo(const Pengo&) = delete;
	Pengo(Pengo&&) noexcept = delete;
	Pengo& operator=(const Pengo&) = delete;
	Pengo& operator=(Pengo&&) noexcept = delete;
};