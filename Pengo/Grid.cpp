#include "MiniginPCH.h"
#include "Grid.h"

Grid::Grid(size_t sizeX, size_t sizeY)
	: m_SizeX{ sizeX }
	, m_SizeY{ sizeY }
	, m_Cells(sizeX * sizeY)
{
}

Grid::~Grid()
{
}