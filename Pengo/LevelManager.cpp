#include "MiniginPCH.h"
#include "LevelManager.h"

LevelManager::LevelManager(const std::string& levelDirectory)
	: m_LevelDirectory{}
	, m_LevelData{}
{
	// Get current directory
	// https://stackoverflow.com/questions/4517425/how-to-get-program-path
	char path[_MAX_PATH + 1];
	GetCurrentDirectory(_MAX_PATH, path);

	std::string pathStr{ &path[0] };
	auto found = pathStr.find_last_of('\\');
	if (found != std::string::npos)
	{
		m_LevelDirectory = pathStr.substr(0, found + 1);
		m_LevelDirectory.append(levelDirectory);
	}
}

void LevelManager::LoadLevels()
{
	std::vector<std::string> fileNames{};

	try
	{
		// Get all files in a directory
		// https://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
		auto dirIt = std::filesystem::directory_iterator(m_LevelDirectory);
		while (!dirIt._At_end())
		{
			const auto& dirEntry = *dirIt;
			if(dirEntry.is_regular_file())
				fileNames.push_back(dirEntry.path().string());
			++dirIt;
		}
	}
	catch (const std::exception& ex)
	{
		rde::Logger::GetInstance()->Log(ex.what(), rde::Logger::LogType::Error);
	}

	if (!fileNames.empty())
	{
		for (size_t i{}; i < fileNames.size(); ++i)
			ParseLevel(fileNames[i], i);
	}
}

void LevelManager::ParseLevel(const std::string& levelFile, size_t index)
{
	std::ifstream inStream{ levelFile };

	if (!inStream)
	{
		std::string logMsg{ "LevelManager::LoadLevel >> Could not open file: " };
		logMsg.append(levelFile);
		rde::Logger::GetInstance()->Log(logMsg.c_str(), rde::Logger::LogType::Error);
		return;
	}

	LevelData newLevel{};
	newLevel.Number = index + 1;

	std::string line{};
	int lvlY = 0;
	while (!inStream.eof())
	{
		inStream >> line;

		if (line.size() != LevelData::LevelSizeX)
		{
			rde::Logger::GetInstance()->Log("LevelManager::LoadLevel >> Invalid level X size format!", rde::Logger::LogType::Error);
			return;
		}

		for (size_t i{}; i < line.size(); ++i)
			newLevel.LoadedData[lvlY][i] = line[i];

		++lvlY;
	}

	if (lvlY != LevelData::LevelSizeY)
	{
		rde::Logger::GetInstance()->Log("LevelManager::LoadLevel >> Invalid level Y size format!", rde::Logger::LogType::Error);
		return;
	}

	m_LevelData.push_back(newLevel);
}