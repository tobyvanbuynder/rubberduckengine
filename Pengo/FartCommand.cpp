#include "MiniginPCH.h"
#include "FartCommand.h"

#include "GameObject.h"
#include "PengoComponent.h"


FartCommand::FartCommand()
	: Command(FartCommandType)
{}

void FartCommand::Execute(rde::GameObject* pGameObj) const
{
	auto pPengoComp = pGameObj->GetComponent<PengoComponent>();

	if (!pPengoComp)
	{
		rde::Logger::GetInstance()->Log("GameObject does not contain PengoComponent. Tried using FartCommand", rde::Logger::LogType::Error);
		return;
	}

	pPengoComp->HandleCommand(FartCommandType);
}