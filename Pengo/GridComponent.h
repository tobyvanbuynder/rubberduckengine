#pragma once

#include "Component.h"

class Grid;
class GameObject;

class GridComponent final : public rde::Component
{
public:
	explicit GridComponent(Grid* pGrid);
	virtual ~GridComponent() = default;

	//int GetIndex() const;

private:
	Grid* m_pGrid;

	//std::vector<GameObject*> 
};