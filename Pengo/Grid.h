#pragma once

class GridCell; // Change this

class Grid final
{
public:
	explicit Grid(size_t sizeX, size_t sizeY);
	~Grid();

	//int GetIndexAbove(int startIdx) const;
	//int GetIndexBelow(int startIdx) const;
	//int GetIndexLeft(int startIdx) const;
	//int GetIndexRight(int startIdx) const;

	//GridCellData GetCellDataFromIndex(int idx) const;

private:
	size_t m_SizeX, m_SizeY;
	std::vector<GridCell> m_Cells;
};