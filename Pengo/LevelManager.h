#pragma once

struct LevelData
{
	static const int LevelSizeX = 13;
	static const int LevelSizeY = 15;

	char LoadedData[LevelData::LevelSizeY][LevelData::LevelSizeX] = { {} };
	size_t Number = 0;
};

class LevelManager final
{
public:
	explicit LevelManager(const std::string& levelDirectory);
	virtual ~LevelManager() = default;

	void LoadLevels();


private:
	std::string m_LevelDirectory;
	std::vector<LevelData> m_LevelData;

	void ParseLevel(const std::string& levelFile, size_t index);

	// Disable copy & move constructor/assignment operator
	LevelManager(const LevelManager&) = delete;
	LevelManager(LevelManager&&) noexcept = delete;
	LevelManager& operator=(const LevelManager&) = delete;
	LevelManager& operator=(LevelManager&&) noexcept = delete;
};

