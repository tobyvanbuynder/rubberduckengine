#pragma once

class PengoState
{
public:
	explicit PengoState() = default;
	virtual ~PengoState() = default;

	virtual void Enter() = 0;
	virtual void Update(float) = 0;
	virtual PengoState* HandleCommand(int command) = 0;

private:

	// Disable copy & move constructor/assignment operator
	PengoState(const PengoState&) = delete;
	PengoState(PengoState&&) noexcept = delete;
	PengoState& operator=(const PengoState&) = delete;
	PengoState& operator=(PengoState&&) noexcept = delete;
};