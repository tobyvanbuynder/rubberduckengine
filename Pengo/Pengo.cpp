#include "MiniginPCH.h"
#include "Pengo.h"

#include "Renderer.h"
#include "SceneManager.h"
#include "InputManager.h"
#include "GameScene.h"
#include "GameObject.h"
#include "TextureComponent.h"
#include "TransformComponent.h"
#include "TextComponent.h"
#include "FPSComponent.h"
#include "PlayerControllerComponent.h"
#include "FartCommand.h"
#include "PengoComponent.h"

Pengo::Pengo()
	: m_LevelManager{ "Data/Levels/" }
{}

void Pengo::Initialize()
{
	// Prog4 assignment W5 test scene

	/// TODO: move this shit
	auto pSceneManager = rde::SceneManager::GetInstance();

	auto pTestScene = new rde::GameScene("Prog4Scene");

	auto pObject = new rde::GameObject();
	pObject->AddComponent(new rde::TextureComponent("../../Data/Textures/background.jpg"));
	pTestScene->AddObject(pObject);

	pObject = new rde::GameObject();
	pObject->AddComponent(new rde::TextureComponent("../../Data/Textures/digdug.png"));
	pObject->GetTransform()->Translate(glm::vec2(216.0f, 192.5f));
	pTestScene->AddObject(pObject);

	pObject = new rde::GameObject();
	pObject->AddComponent(new rde::FPSComponent());
	pObject->AddComponent(new rde::TextComponent("../../Data/Fonts/Lingua.otf", 16, "Text", SDL_Color{ 255, 255, 0, 255 }));
	pObject->GetTransform()->Translate(glm::vec2(5.0f, 5.0f));
	pTestScene->AddObject(pObject);

	pObject = new rde::GameObject();
	pObject->AddComponent(new rde::TextComponent("../../Data/Fonts/Lingua.otf", 36, "I'll do better in August, I swear"));
	pObject->GetTransform()->Translate(glm::vec2(80.0f, 30.0f));
	//
	pObject->AddComponent(new rde::PlayerControllerComponent(rde::InputManager::Player1));
	pObject->AddComponent(new PengoComponent());
	//
	pTestScene->AddObject(pObject);

	// Load all levels
	m_LevelManager.LoadLevels();

	// SceneManager adding inspired by OverlordEngine
	pSceneManager->AddScene(pTestScene);
	pSceneManager->SetActiveScene("Prog4Scene");
}