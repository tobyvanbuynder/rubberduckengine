#pragma once

#include "Component.h"

class PengoComponent final : public rde::Component
{
public:
	explicit PengoComponent() = default;
	virtual ~PengoComponent() = default;

	virtual void Initialize() override;
	virtual void Update(float) override;

	void HandleCommand(int command);

private:
	// Disable copy & move constructor/assignment operator
	PengoComponent(const PengoComponent&) = delete;
	PengoComponent(PengoComponent&&) noexcept = delete;
	PengoComponent& operator=(const PengoComponent&) = delete;
	PengoComponent& operator=(PengoComponent&&) noexcept = delete;
};