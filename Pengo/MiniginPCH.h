#pragma once

#include "targetver.h"

#include <stdio.h>
#include <iostream> // std::cout
#include <sstream> // stringstream
#include <fstream> // filestream
#include <memory> // smart pointers
// Containers
#include <vector>
#include <map>
#include <array>
// ---
#include <chrono> // timing
#include <iomanip> // input/output manipulation
#include <algorithm> // algorithms
#include <filesystem> // C++17 filesystem


#include <vld.h> // Visual Leak Debugger


#define WIN32_LEAN_AND_MEAN
#include <windows.h>


// GLMath
#pragma warning(push)
#pragma warning (disable:4201)
#include "glm/glm.hpp"
#pragma warning(pop)


// SDL stuff
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include "Logger.h" // Logger for file/debug