#pragma once

#include "Command.h"

class FartCommand final : public rde::Command
{
public:
	explicit FartCommand();
	virtual ~FartCommand() = default;

	virtual void Execute(rde::GameObject* pGameObj) const override;
	
	static const int FartCommandType = 100;

private:
	// Disable copy & move constructor/assignment operator
	FartCommand(const FartCommand&) = delete;
	FartCommand(FartCommand&&) noexcept = delete;
	FartCommand& operator=(const FartCommand&) = delete;
	FartCommand& operator=(FartCommand&&) noexcept = delete;
};