#pragma once

#include "Component.h"

namespace rde
{
	class PlayerControllerComponent final : public Component
	{
	public:
		explicit PlayerControllerComponent(UINT controllerID);
		virtual ~PlayerControllerComponent();

		virtual void Initialize() override;
		virtual void Update(float) override;

	private:
		UINT m_ControllerID;

		// Disable copy & move constructor/assignment operator
		PlayerControllerComponent(const PlayerControllerComponent&) = delete;
		PlayerControllerComponent(PlayerControllerComponent&&) noexcept = delete;
		PlayerControllerComponent& operator=(const PlayerControllerComponent&) = delete;
		PlayerControllerComponent& operator=(PlayerControllerComponent&&) noexcept = delete;
	};
}