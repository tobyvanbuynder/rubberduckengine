#include "MiniginPCH.h"
#include "FPSComponent.h"

#include "TextComponent.h"
#include "GameObject.h"


rde::FPSComponent::FPSComponent()
	: Component()
	, m_pTextComp{ nullptr }
{
}

rde::FPSComponent::~FPSComponent()
{
}


void rde::FPSComponent::Initialize()
{
	m_pTextComp = GetGameObject()->GetComponent<TextComponent>();
	if (!m_pTextComp)
		Logger::GetInstance()->Log("FPSComponent::Initialize error > GameObject has no TextComponent!", Logger::LogType::Error);
}

void rde::FPSComponent::Update(float deltaTime)
{
	// Calculate FPS and set as new text
	m_pTextComp->SetText(std::to_string(int(1 / deltaTime)) + " FPS");
}