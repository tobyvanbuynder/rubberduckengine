 #include "MiniginPCH.h"
#include "TextureComponent.h"

#include "Renderer.h"
#include "GameObject.h"
#include "TransformComponent.h"


rde::TextureComponent::TextureComponent(const std::string& filePath)
	: Component()
	, m_pTexture{ nullptr }
	, m_FilePath{ filePath }
	, m_Width{ }
	, m_Height{ }
	, m_DestRect{ }
	, m_SrcRect{ }
{
}

rde::TextureComponent::~TextureComponent()
{
}


void rde::TextureComponent::Initialize()
{
	if (!m_FilePath.empty())
	{
		// Load texture through Renderer
		m_pTexture = Renderer::GetInstance()->LoadTexture(m_FilePath);

		// Grab width and height from texture, inspired by:
		// https://www.willusher.io/sdl2%20tutorials/2013/08/18/lesson-3-sdl-extension-libraries
		SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_Width, &m_Height);
	}
}

void rde::TextureComponent::Update(float)
{
	// Grab position from GameObject for now
	glm::vec2 pos = GetGameObject()->GetTransform()->GetPosition();

	// Destination to render texture to
	m_DestRect.w = m_Width;
	m_DestRect.h = m_Height;
	m_DestRect.x = int(pos.x);
	m_DestRect.y = int(pos.y);

	// Src Rect
	m_SrcRect.w = m_Width;
	m_SrcRect.h = m_Height;
	m_SrcRect.x = 0;
	m_SrcRect.y = 0;

	// Add to queue
	Renderer::GetInstance()->QueueTexture(m_pTexture, m_DestRect, m_SrcRect);
}