#include "MiniginPCH.h"
#include "Logger.h"

rde::Logger::Logger()
	: m_LogMode{ LogMode::None }
{
}

rde::Logger::~Logger()
{
	if (m_LogMode == LogMode::File && m_FileOutputStream.is_open())
		m_FileOutputStream.close();
}

void rde::Logger::SetLogMode(const LogMode& logMode)
{
	// Return if the type is the same anyway
	if (logMode == m_LogMode)
		return;

	auto finalLogMode = logMode;

	/// TODO: move this to a new function
	// Open/close log file
	if (logMode == LogMode::File)
	{
		// Get the current time in string format
		time_t currentTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		tm localTime;
		localtime_s(&localTime, &currentTime);
		std::string fileName{};
		fileName.resize(26);
		asctime_s(&fileName[0], fileName.size(), &localTime);

		// Cut the \0 and \n characters at the end
		fileName.pop_back();
		fileName.pop_back();

		// Replace all invalid characters
		std::replace(fileName.begin(), fileName.end(), ' ', '_');
		std::replace(fileName.begin(), fileName.end(), ':', '-');

		// Create the new log file
		m_FileOutputStream.open("Log_" + fileName + ".log", std::ios::out | std::ios::app);
		if (!m_FileOutputStream.is_open())
		{
			m_LogMode = LogMode::Debug;
			finalLogMode = LogMode::Debug;

			Log(("Failed to create log file Log_" + fileName + ".log\n").c_str(), LogType::Error);
		}
	}
	else
	{
		// Close the already opened log
		m_FileOutputStream.close();
	}

	// Change type
	m_LogMode = finalLogMode;
}

void rde::Logger::Log(const char* pLogText, const LogType& logType)
{
	// Accumulate message
	std::string logMsg{ "[LOGGER] " };
	switch (logType)
	{
	case LogType::Warning:
		logMsg.append("Warning >> ");
		break;

	case LogType::Error:
		logMsg.append("Error >> ");
		break;
	}
	logMsg.append(pLogText);
	logMsg.append("\n");

	// Output message
	switch (m_LogMode)
	{
		// Write to debug output
	case LogMode::Debug:
		std::cout << logMsg;
		break;

		// Write to file
	case LogMode::File:
		m_FileOutputStream << logMsg;
		break;
	}
}