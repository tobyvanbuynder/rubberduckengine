#pragma once

namespace rde
{
	class GameObject;

	class GameScene
	{
	public:
		explicit GameScene(const std::string& name);
		~GameScene();

		virtual void Initialize();
		virtual void Update(float deltaTime);

		void AddObject(GameObject* pObj);

		virtual void Load() const;
		virtual void Unload() const;

		std::string GetName() const;

	private:
		std::vector<GameObject*> m_GameObjects;
		std::string m_Name;
		bool m_IsInitialized;

		// Disable copy & move constructor/assignment operator
		GameScene(const GameScene&) = delete;
		GameScene(GameScene&&) noexcept = delete;
		GameScene& operator=(const GameScene&) = delete;
		GameScene& operator=(GameScene&&) noexcept = delete;
	};
}