#include "MiniginPCH.h"
#include "Renderer.h"

#include "TextureComponent.h"

rde::Renderer::Renderer()
	: m_pSDLWindow{ nullptr }
	, m_pSDLRenderer{ nullptr }
	, m_WindowSettings{ }
	, m_RenderSettings{ }
{
}

rde::Renderer::~Renderer()
{
	Cleanup();
}

void rde::Renderer::ClearQueue()
{
	m_RenderQueue.clear();
}
void rde::Renderer::QueueTexture(SDL_Texture* pTexture, const SDL_Rect& destRect, const SDL_Rect& srcRect)
{
	TextureData texData{};

	texData.pTexture = pTexture;
	texData.DestRect = destRect;
	texData.SrcRect = srcRect;

	m_RenderQueue.push_back(texData);
}
void rde::Renderer::RenderQueue() const
{
	// Clear the screen
	SDL_RenderClear(m_pSDLRenderer);

	// Render all queue'd textures
	TextureData texData;
	for (size_t i{}; i < m_RenderQueue.size(); ++i)
	{
		texData = m_RenderQueue[i];
		SDL_RenderCopy(m_pSDLRenderer, texData.pTexture, &texData.SrcRect, &texData.DestRect);
	}

	// Present backbuffer
	SDL_RenderPresent(m_pSDLRenderer);
}

void rde::Renderer::Initialize(const WindowSettings& windowSettings, const RenderSettings& renderSettings)
{
	// Apply settings
	m_WindowSettings = windowSettings;
	m_RenderSettings = renderSettings;


	// Initialization code inspired by:
	// https://www.willusher.io/sdl2%20tutorials/2013/08/18/lesson-3-sdl-extension-libraries

	// Init SDL (only rendering and events for now)
	//if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0)
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::string logMsg{ "SDL_Init error > " };
		logMsg.append(SDL_GetError());
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);
		return;
	}

	// Init SDL_Image (only JPG and PNG for now)
	int imgFlags{ IMG_INIT_JPG | IMG_INIT_PNG };
	if ((IMG_Init(imgFlags) & imgFlags) != imgFlags)
	{
		std::string logMsg{ "IMG_Init error > " };
		logMsg.append(SDL_GetError());
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);

		SDL_Quit();
		return;
	}

	// Init SDL_TTF
	if (TTF_Init() != 0)
	{
		std::string logMsg{ "TTF_Init error > " };
		logMsg.append(SDL_GetError());
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);

		IMG_Quit();
		SDL_Quit();
		return;
	}

	// Init Window
	m_pSDLWindow = SDL_CreateWindow(m_WindowSettings.WindowName.c_str(), m_WindowSettings.XPosition, m_WindowSettings.YPosition,
		m_WindowSettings.Width, m_WindowSettings.Height, m_WindowSettings.SDLWindowFlags);
	if (!m_pSDLWindow)
	{
		std::string logMsg{ "SDL_Window error > " };
		logMsg.append(SDL_GetError());
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);

		TTF_Quit();
		IMG_Quit();
		SDL_Quit();
		return;
	}

	// Init Renderer
	m_pSDLRenderer = SDL_CreateRenderer(m_pSDLWindow, m_RenderSettings.Index, m_RenderSettings.SDLRenderFlags);
	if (!m_pSDLRenderer)
	{
		std::string logMsg{ "SDL_Renderer error > " };
		logMsg.append(SDL_GetError());
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);

		SDL_DestroyWindow(m_pSDLWindow);
		TTF_Quit();
		IMG_Quit();
		SDL_Quit();
		return;
	}
}
void rde::Renderer::Cleanup()
{
	// Delete all loaded textures
	auto it = m_LoadedTextures.begin();
	while (it != m_LoadedTextures.cend())
	{
		SDL_DestroyTexture(it->second);
		++it;
	}

	// Destroy SDL renderer
	if(m_pSDLRenderer)
		SDL_DestroyRenderer(m_pSDLRenderer);

	// Destroy SDL window
	if(m_pSDLWindow)
		SDL_DestroyWindow(m_pSDLWindow);

	// Destroy SDL libraries
	TTF_Quit();
	IMG_Quit();

	// Destroy SDL
	SDL_Quit();
}


SDL_Texture* rde::Renderer::CreateTextureFromSurface(SDL_Surface* pSurface) const
{
	SDL_Texture* pTexture{ SDL_CreateTextureFromSurface(m_pSDLRenderer, pSurface) };

	if (!pTexture)
	{
		std::string logMsg{ "Renderer::CreateTextureFromSurface > " };
		logMsg.append(SDL_GetError());
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);
		return nullptr;
	}

	return pTexture;
}
SDL_Texture* rde::Renderer::LoadTexture(const std::string& filePath)
{
	// Use SDL_Texture instead of SDL_Surface because SDL_Texture is GPU-level
	// https://stackoverflow.com/questions/21392755/difference-between-surface-and-texture-sdl-general

	// Get path to this process' working directory
	// https://wiki.libsdl.org/SDL_GetBasePath
	std::string finalPath{ SDL_GetBasePath() };

	// Make sure it loads the file
	finalPath.append(filePath);

	// Return already loaded texture if found
	auto foundIt = m_LoadedTextures.find(filePath);
	if (foundIt != m_LoadedTextures.cend())
		return m_LoadedTextures[filePath];

	// Load texture from path
	SDL_Texture* pTexture{ IMG_LoadTexture(m_pSDLRenderer, finalPath.c_str()) };

	if (!pTexture)
	{
		std::string logMsg{ "Renderer::LoadTexture > " };
		logMsg.append(SDL_GetError());
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);
		return nullptr;
	}

	m_LoadedTextures[filePath] = pTexture;

	/// TODO: Resilience -> Create "fail" texture
	// https://wiki.libsdl.org/SDL_CreateTexture
	// http://lazyfoo.net/tutorials/SDL/40_texture_manipulation/index.php
	// https://wiki.libsdl.org/SDL_CreateTextureFromSurface <- check this, possibly what I'm looking for
	//if (!pTexture)
	//{
	//	pTexture = SDL_CreateTexture(m_pSDLRenderer, SDL_GetWindowPixelFormat(m_pSDLWindow), SDL_TEXTUREACCESS_STATIC, 16, 16);
	//
	//	void* pPixels{ nullptr };
	//	int* pPitch{ nullptr };
	//	SDL_LockTexture(pTexture, nullptr, &pPixels, pPitch);
	//}

	return pTexture;
}

rde::WindowSettings rde::Renderer::GetWindowSettings() const
{
	return m_WindowSettings;
}
rde::RenderSettings rde::Renderer::GetRenderSettings() const
{
	return m_RenderSettings;
}
void rde::Renderer::SetWindowSettings(const WindowSettings& windowSettings)
{
	// Dynamic window settings
	m_WindowSettings = windowSettings;

	SDL_SetWindowPosition(m_pSDLWindow, windowSettings.XPosition, windowSettings.YPosition);
	SDL_SetWindowSize(m_pSDLWindow, windowSettings.Width, windowSettings.Height);
	SDL_SetWindowBordered(m_pSDLWindow, SDL_bool((windowSettings.SDLWindowFlags & SDL_WINDOW_BORDERLESS) != SDL_WINDOW_BORDERLESS));
	SDL_SetWindowFullscreen(m_pSDLWindow, SDL_bool((windowSettings.SDLWindowFlags & SDL_WINDOW_FULLSCREEN) == SDL_WINDOW_FULLSCREEN));
	SDL_SetWindowTitle(m_pSDLWindow, windowSettings.WindowName.c_str());
}
void rde::Renderer::SetRenderSettings(const RenderSettings& renderSettings)
{
	// Dynamic render settings
	m_RenderSettings = renderSettings;

	SDL_GL_SetSwapInterval((renderSettings.SDLRenderFlags & SDL_RENDERER_PRESENTVSYNC) == SDL_RENDERER_PRESENTVSYNC);
}