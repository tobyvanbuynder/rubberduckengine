#include "MiniginPCH.h"
#include "RubberDuckEngine.h"

#include "Logger.h"
#include "Renderer.h"
#include "InputManager.h"
#include "SceneManager.h"
#include "FontLoader.h"

#include "Game.h"

rde::RubberDuckEngine::RubberDuckEngine()
	: m_pGame{ nullptr }
{
}

rde::RubberDuckEngine::~RubberDuckEngine()
{
}


void rde::RubberDuckEngine::Run()
{
	auto* pLogger = Logger::GetInstance();

	if (!m_pGame)
	{
		pLogger->SetLogMode(Logger::LogMode::Debug);
		pLogger->Log("RubberDuckEngine::Run() > No Game set in engine!", Logger::LogType::Error);
		return;
	}

	// Save and initialize core
	auto* pInputManager = InputManager::GetInstance();
	auto* pRenderer = Renderer::GetInstance();

	// Initialize FontLoader
	FontLoader::GetInstance();

	// Init Game
	m_pGame->Initialize();

	auto* pSceneManager = SceneManager::GetInstance();

	// Init scenes
	pSceneManager->Initialize();

	// Game Loop
	// http://gameprogrammingpatterns.com/game-loop.html
	// https://gafferongames.com/post/fix_your_timestep/
	auto currentTime = std::chrono::high_resolution_clock::now();
	SDL_Event e;
	bool exit{ false };
	float lag{};
	while (!exit)
	{
		// Grab deltaTime between frames
		auto newTime = std::chrono::high_resolution_clock::now();

		// Get duration in float
		// https://stackoverflow.com/questions/14391327/how-to-get-duration-as-int-millis-and-float-seconds-from-chrono
		auto elapsedSec = std::chrono::duration<float>(newTime - currentTime).count();
		// Add to lag
		lag += elapsedSec;

		// Go over the event queue and process each event
		while (SDL_PollEvent(&e) != 0)
		{
			switch (e.type)
			{
				// Engine
			case SDL_QUIT:
				exit = true;
				break;
			case RDE_SCENESWITCHEVENT: // Custom event
				pSceneManager->SwitchScene();
				break;

				// Input
				//Keyboard
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				pInputManager->ProcessKeyboard(e);
				break;

				//Controller
			case SDL_CONTROLLERAXISMOTION:
			case SDL_CONTROLLERBUTTONDOWN:
			case SDL_CONTROLLERBUTTONUP:
				pInputManager->ProcessController(e);
				break;

			case SDL_CONTROLLERDEVICEADDED:
				pInputManager->OnControllerOpen(e.cdevice.which);
				pLogger->Log("Controller got added\n");
				break;
			case SDL_CONTROLLERDEVICEREMOVED:
				pInputManager ->OnControllerClose(e.cdevice.which);
				pLogger->Log("Controller got removed\n");
				break;
			}
		}

		while (lag >= m_MsPerUpdate)
		{
			// Clear renderqueue
			pRenderer->ClearQueue();

			// Update scene
			pSceneManager->Update(m_MsPerUpdate);

			// Update lag
			lag -= m_MsPerUpdate;
		}
		
		// Render the queue
		pRenderer->RenderQueue();

		// Reset time passed
		currentTime = newTime;
	}
}

void rde::RubberDuckEngine::Initialize(WindowSettings windowSettings, RenderSettings renderSettings)
{
	// Init stuff
	Renderer::GetInstance()->Initialize(windowSettings, renderSettings);
	InputManager::GetInstance();
	Logger::GetInstance()->SetLogMode(Logger::LogMode::Debug);
	SceneManager::GetInstance();
	FontLoader::GetInstance();
}

void rde::RubberDuckEngine::Cleanup()
{
	FontLoader::DeleteInstance();
	SceneManager::DeleteInstance();
	Logger::DeleteInstance();
	InputManager::DeleteInstance();
	Renderer::DeleteInstance();
}

void rde::RubberDuckEngine::SetGame(Game* pGame)
{
	m_pGame = pGame;
}