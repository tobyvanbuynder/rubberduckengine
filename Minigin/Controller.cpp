#include "MiniginPCH.h"
#include "Controller.h"

#include "Logger.h"
#include "Command.h"

// Controller data
//http://lazyfoo.net/tutorials/SDL/19_gamepads_and_joysticks/index.php

rde::Controller::Controller()
{
}

rde::Controller::~Controller()
{
	// Delete all commands
	for (const std::pair<ControllerButtonData, Command*>& mapPair : m_InputMap)
		delete mapPair.second;
}


void rde::Controller::ProcessButton(const SDL_ControllerButtonEvent& event, const std::vector<GameObject*>& controlledGameObjects) const
{
	// Get controller button info
	ControllerButtonData buttonData{};
	buttonData.ControllerID = event.which;
	buttonData.Button = static_cast<Controller::Button>(event.button);
	buttonData.State =
		event.state == SDL_PRESSED
		? ButtonState::PRESSED
		: ButtonState::RELEASED;
	buttonData.Type =
		event.type == SDL_CONTROLLERBUTTONDOWN 
		? ButtonType::DOWN
		: ButtonType::UP;
	
	// If it's in the inputmap
	auto findIt = m_InputMap.find(buttonData);
	if (findIt != m_InputMap.end())
	{
		// Execute command for all linked GameObjects
		for (size_t i{}; i < controlledGameObjects.size(); ++i)
			findIt->second->Execute(controlledGameObjects[i]);
	}

	// Test Logger
	switch (buttonData.Button)
	{
	case Controller::Button::A:
		Logger::GetInstance()->Log("[A]");
		break;
	case Controller::Button::B:
		Logger::GetInstance()->Log("[B]");
		break;
	case Controller::Button::X:
		Logger::GetInstance()->Log("[X]");
		break;
	case Controller::Button::Y:
		Logger::GetInstance()->Log("[Y]");
		break;
	case Controller::Button::START:
		Logger::GetInstance()->Log("[START]");
		break;
	case Controller::Button::BACK:
		Logger::GetInstance()->Log("[BACK]");
		break;
	}
	Logger::GetInstance()->Log("\n");
}

void rde::Controller::MapCommand(const ControllerButtonData& button, Command* pCommand)
{
	m_InputMap[button] = pCommand;
}

void rde::Controller::OpenController(int controllerID)
{
	m_SDLControllerID = controllerID;
	m_pSDLGameController = SDL_GameControllerOpen(controllerID);
}

void rde::Controller::CloseController() const
{
	SDL_GameControllerClose(m_pSDLGameController);
}