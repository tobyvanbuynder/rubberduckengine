#pragma once

#include "Singleton.h"
#include "TextureComponent.h"

namespace rde
{
	// Helper structs
	struct WindowSettings
	{
		int XPosition, YPosition;
		int Width, Height;
		Uint32 SDLWindowFlags;
		std::string WindowName;
	};
	struct RenderSettings
	{
		int Index;
		Uint32 SDLRenderFlags;
	};

	struct TextureData
	{
		SDL_Texture* pTexture = nullptr;
		SDL_Rect DestRect = {};
		SDL_Rect SrcRect = {};
	};


	class Renderer final : public Singleton<Renderer>
	{
	public:
		explicit Renderer();
		~Renderer();

		// Core rendering
		void ClearQueue();
		void QueueTexture(SDL_Texture*, const SDL_Rect&, const SDL_Rect&);
		void RenderQueue() const;


		// Call required after Singleton construction
		void Initialize(const WindowSettings& windowSettings, const RenderSettings& renderSettings);

		// Load SDL textures
		SDL_Texture* CreateTextureFromSurface(SDL_Surface* pSurface) const;
		SDL_Texture* LoadTexture(const std::string& filePath);

		// Getters/Setters
		WindowSettings GetWindowSettings() const;
		RenderSettings GetRenderSettings() const;
		void SetWindowSettings(const WindowSettings& windowSettings);
		void SetRenderSettings(const RenderSettings& renderSettings);

	private:
		SDL_Window* m_pSDLWindow;
		SDL_Renderer* m_pSDLRenderer;
		WindowSettings m_WindowSettings;
		RenderSettings m_RenderSettings;

		std::map<std::string, SDL_Texture*> m_LoadedTextures;
		std::vector<TextureData> m_RenderQueue;

		void Cleanup();

		// Disable copy & move constructor/assignment operator
		Renderer(const Renderer&) = delete;
		Renderer(Renderer&&) noexcept = delete;
		Renderer& operator=(const Renderer&) = delete;
		Renderer& operator=(Renderer&&) noexcept = delete;
	};
}