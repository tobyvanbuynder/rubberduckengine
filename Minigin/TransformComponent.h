#pragma once
#include "Component.h"

namespace rde
{
	class TransformComponent final : public Component
	{
	public:
		explicit TransformComponent();
		virtual ~TransformComponent();

		void Initialize() override;
		void Update(float deltaTime) override;

		void Translate(const glm::vec2& translate);
		void Rotate(const glm::vec2& rotate);
		void Scale(const glm::vec2& scale);

		glm::vec2 GetPosition() const;
		glm::vec2 GetPreviousPosition() const;
		glm::vec2 GetRotation() const;
		glm::vec2 GetScale() const;

		void SetPosition(const glm::vec2& pos);
		void SetRotation(const glm::vec2& rot);
		void SetScale(const glm::vec2& scale);

	private:
		glm::vec2 m_Position, m_PrevPosition;
		glm::vec2 m_Rotation;
		glm::vec2 m_Scale;

		// Disable copy & move constructor/assignment operator
		TransformComponent(const TransformComponent&) = delete;
		TransformComponent(TransformComponent&&) noexcept = delete;
		TransformComponent& operator=(const TransformComponent&) = delete;
		TransformComponent& operator=(TransformComponent&&) noexcept = delete;
	};
}