#pragma once
#include "Renderer.h"

namespace rde
{
	class Game;

	class RubberDuckEngine
	{
	public:
		explicit RubberDuckEngine();
		~RubberDuckEngine();

		void Run();
		void Initialize(WindowSettings windowSettings, RenderSettings renderSettings);
		void Cleanup();

		void SetGame(Game* pGame);

	private:
		Game* m_pGame;
		const float m_MsPerUpdate = 1 / 60.0f; // Runs at 60FPS

		// Disable copy & move constructor/assignment operator
		RubberDuckEngine(const RubberDuckEngine&) = delete;
		RubberDuckEngine(RubberDuckEngine&&) noexcept = delete;
		RubberDuckEngine& operator=(const RubberDuckEngine&) = delete;
		RubberDuckEngine& operator=(RubberDuckEngine&&) noexcept = delete;
	};
}