#pragma once
#include "Singleton.h"

namespace rde
{
	class GameScene;

	static const Uint32 RDE_SCENESWITCHEVENT{ 9000 };

	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		explicit SceneManager();
		~SceneManager();

		void Initialize();
		void Update(float deltaTime);

		void AddScene(GameScene* pScene);
		void SetActiveScene(const std::string& name);

		void SwitchScene();

	private:
		std::vector<GameScene*> m_Scenes;
		GameScene *m_pActiveScene, *m_pPrevScene;

		GameScene* GetSceneFromName(const std::string& name) const;

		// Disable copy & move constructor/assignment operator
		SceneManager(const SceneManager&) = delete;
		SceneManager(SceneManager&&) noexcept = delete;
		SceneManager& operator=(const SceneManager&) = delete;
		SceneManager& operator=(SceneManager&&) noexcept = delete;
	};
}