#pragma once

#include "Component.h"

namespace rde
{
	class TextureComponent;

	class TextComponent final : public Component
	{
	public:
		explicit TextComponent(const std::string& filePath, int size = 12, const std::string& text = "TextComesHere", const SDL_Color& color = { 255, 255, 255, 255 });
		virtual ~TextComponent();

		virtual void Initialize() override;
		virtual void Update(float deltaTime) override;

		void SetText(const std::string& text);
		void SetColor(const SDL_Color& color);

	private:
		TTF_Font* m_pFont;
		SDL_Texture* m_pTexture;
		std::string m_FilePath;
		int m_Size;
		std::string m_Text;
		SDL_Color m_Color;
		bool m_NeedsUpdate;
		SDL_Rect m_DestRect, m_SrcRect;

		// Disable copy & move constructor/assignment operator
		TextComponent(const TextComponent&) = delete;
		TextComponent(TextComponent&&) noexcept = delete;
		TextComponent& operator=(const TextComponent&) = delete;
		TextComponent& operator=(TextComponent&&) noexcept = delete;
	};
}