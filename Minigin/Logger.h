#pragma once

#include "Singleton.h"

namespace rde
{
	class Logger final : public Singleton<Logger>
	{
	public:
		enum class LogMode
		{
			None, Debug, File
		};
		enum class LogType
		{
			Normal, Warning, Error
		};

		explicit Logger();
		~Logger();

		void SetLogMode(const LogMode& logMode);
		void Log(const char* pLogText, const LogType& logType = LogType::Normal);

	private:
		LogMode m_LogMode;
		std::ofstream m_FileOutputStream;

		// Disable copy & move constructor/assignment operator
		Logger(const Logger&) = delete;
		Logger(Logger&&) noexcept = delete;
		Logger& operator=(const Logger&) = delete;
		Logger& operator=(Logger&&) noexcept = delete;
	};
}