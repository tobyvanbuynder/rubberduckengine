#include "MiniginPCH.h"
#include "Command.h"

rde::Command::Command(int type)
	: m_CommandType{ type }
{}

int rde::Command::GetCommandType() const
{
	return m_CommandType;
};