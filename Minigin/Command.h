#pragma once

namespace rde
{
	class GameObject;

	class Command
	{
	public:
		explicit Command(int type);
		virtual ~Command() = default;

		virtual void Execute(GameObject* pGameObj) const = 0;

		int GetCommandType() const;

	private:
		int m_CommandType;

		// Disable copy & move constructor/assignment operator
		Command(const Command&) = delete;
		Command(Command&&) noexcept = delete;
		Command& operator=(const Command&) = delete;
		Command& operator=(Command&&) noexcept = delete;
	};
}