#include "MiniginPCH.h"
#include "TransformComponent.h"


rde::TransformComponent::TransformComponent()
	: Component()
	, m_Position{ 0,0 }
	, m_PrevPosition{ 0,0 }
	, m_Rotation{ 0,0 }
	, m_Scale{ 0,0 }
{
}

rde::TransformComponent::~TransformComponent()
{
}


void rde::TransformComponent::Initialize()
{
}

void rde::TransformComponent::Update(float)
{
	m_PrevPosition = m_Position;
}


void rde::TransformComponent::Translate(const glm::vec2& translate)
{
	m_Position += translate;
}
void rde::TransformComponent::Rotate(const glm::vec2& rotate)
{
	m_Rotation += rotate;
}
void rde::TransformComponent::Scale(const glm::vec2& scale)
{
	m_Scale += scale;
}

glm::vec2 rde::TransformComponent::GetPosition() const
{
	return m_Position;
}
glm::vec2 rde::TransformComponent::GetPreviousPosition() const
{
	return m_PrevPosition;
}
glm::vec2 rde::TransformComponent::GetRotation() const
{
	return m_Rotation;
}
glm::vec2 rde::TransformComponent::GetScale() const
{
	return m_Scale;
}

void rde::TransformComponent::SetPosition(const glm::vec2& pos)
{
	m_Position = pos;
}
void rde::TransformComponent::SetRotation(const glm::vec2& rot)
{
	m_Rotation = rot;
}
void rde::TransformComponent::SetScale(const glm::vec2& scale)
{
	m_Scale = scale;
}