#pragma once

namespace rde
{
	class GameObject;

	class Component
	{
	public:
		explicit Component();
		virtual ~Component();

		virtual void Initialize() = 0;
		virtual void Update(float deltaTime) = 0;

		virtual void Load() {};
		virtual void Unload() {};

		GameObject* GetGameObject() const;

	private:
		GameObject* m_pOwningObject;


		// Ability to link GameObject to Component, nasty link tho
		friend class GameObject;
	};
}