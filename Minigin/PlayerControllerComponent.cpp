#include "MiniginPCH.h"
#include "PlayerControllerComponent.h"

#include "InputManager.h"

rde::PlayerControllerComponent::PlayerControllerComponent(UINT controllerID)
	: Component()
	, m_ControllerID(controllerID)
{
}

rde::PlayerControllerComponent::~PlayerControllerComponent()
{
	InputManager::GetInstance()->RemoveControlledObjectFromPlayer(m_ControllerID, GetGameObject());
}


void rde::PlayerControllerComponent::Initialize()
{
	InputManager::GetInstance()->AddControlledObjectToPlayer(m_ControllerID, GetGameObject());
}

void rde::PlayerControllerComponent::Update(float)
{
}