#pragma once

#include "Component.h"

namespace rde
{
	class TextureComponent final : public Component
	{
	public:
		explicit TextureComponent(const std::string& filePath);
		~TextureComponent();

		virtual void Initialize() override;
		virtual void Update(float deltaTime) override;

		// TODO: ADD SCALING AND PIVOT

	private:
		SDL_Texture* m_pTexture;
		std::string m_FilePath;
		int m_Width;
		int m_Height;
		SDL_Rect m_DestRect, m_SrcRect;

		// Disable copy & move constructor/assignment operator
		TextureComponent(const TextureComponent&) = delete;
		TextureComponent(TextureComponent&&) noexcept = delete;
		TextureComponent& operator=(const TextureComponent&) = delete;
		TextureComponent& operator=(TextureComponent&&) noexcept = delete;

		// Ability to link TextComponent to TextureComponent, nasty link tho
		friend class TextComponent;
	};
}