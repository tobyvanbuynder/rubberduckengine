#include "MiniginPCH.h"
#include "FontLoader.h"


rde::FontLoader::~FontLoader()
{
	// Every font path
	auto fontPathIt = m_LoadedFonts.begin();
	while (fontPathIt != m_LoadedFonts.cend())
	{
		// Every font size
		auto fontSizeIt = fontPathIt->second.begin();
		while (fontSizeIt != fontPathIt->second.cend())
		{
			// Release the font
			TTF_CloseFont(fontSizeIt->second);
			++fontSizeIt;
		}
		++fontPathIt;
	}
}

TTF_Font* rde::FontLoader::LoadFont(const std::string& pathToFont, int fontSize)
{
	// Get path to this process' working directory
	// https://wiki.libsdl.org/SDL_GetBasePath
	std::string finalPath{ SDL_GetBasePath() };

	// Make sure it loads the file
	finalPath.append(pathToFont);
	
	// Check if it's already loaded
	auto findIt = m_LoadedFonts.find(finalPath);
	if (findIt != m_LoadedFonts.cend())
	{
		// Check if the size already exists
		auto findSizeIt = findIt->second.find(fontSize);
		if (findSizeIt != findIt->second.cend())
			return findSizeIt->second;

		// Create size font
		auto* pNewSizeFont = findIt->second[fontSize] = TTF_OpenFont(finalPath.c_str(), fontSize);
		return pNewSizeFont;
	}

	// Create a new map
	std::map<int, TTF_Font*> newMap{};

	// Add the font to it
	auto* pNewFont = newMap[fontSize] = TTF_OpenFont(finalPath.c_str(), fontSize);
	m_LoadedFonts[finalPath] = newMap;
	return pNewFont;
}