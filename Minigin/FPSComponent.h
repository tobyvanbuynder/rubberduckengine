#pragma once

#include "Component.h"
namespace rde
{
	class TextComponent;

	class FPSComponent final : public Component
	{
	public:
		explicit FPSComponent();
		~FPSComponent();

		virtual void Initialize() override;
		virtual void Update(float deltaTime) override;

	private:
		TextComponent* m_pTextComp;

		// Disable copy & move constructor/assignment operator
		FPSComponent(const FPSComponent&) = delete;
		FPSComponent(FPSComponent&&) noexcept = delete;
		FPSComponent& operator=(const FPSComponent&) = delete;
		FPSComponent& operator=(FPSComponent&&) noexcept = delete;
	};
}