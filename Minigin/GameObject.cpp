#include "MiniginPCH.h"
#include "GameObject.h"

#include "TransformComponent.h"
#include "TextureComponent.h"

rde::GameObject::GameObject()
	: m_pTransformComponent{ new TransformComponent() }
	, m_IsInitialized{ false }
{
	AddComponent(m_pTransformComponent);
}

rde::GameObject::~GameObject()
{
	for (size_t i{}; i < m_Components.size(); ++i)
		delete m_Components[i];
}


void rde::GameObject::Initialize()
{
	if (m_IsInitialized)
		return;

	for (size_t i{}; i < m_Components.size(); ++i)
		m_Components[i]->Initialize();

	m_IsInitialized = true;
}

void rde::GameObject::Update(float deltaTime)
{
	for (size_t i{}; i < m_Components.size(); ++i)
		m_Components[i]->Update(deltaTime);
}


void rde::GameObject::Load() const
{
	for (size_t i{}; i < m_Components.size(); ++i)
		m_Components[i]->Load();
}
void rde::GameObject::Unload() const
{
	for (size_t i{}; i < m_Components.size(); ++i)
		m_Components[i]->Unload();
}


void rde::GameObject::AddComponent(Component* pComp)
{
	if (std::find(m_Components.cbegin(), m_Components.cend(), pComp) == m_Components.cend())
	{
		pComp->m_pOwningObject = this;
		m_Components.push_back(pComp);
	}
}


rde::TransformComponent* rde::GameObject::GetTransform() const
{
	return m_pTransformComponent;
}