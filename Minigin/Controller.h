#pragma once

#include "Component.h"

#include <map>
#include "InputButtonStructs.h"

namespace rde
{
	class Command;
	class GameObject;
	struct ControllerButtonData;

	class Controller final
	{
	public:
		enum class Button
		{
			// Buttons
			A = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_A,
			B = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_B,
			X = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_X,
			Y = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_Y,
			BACK = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_BACK,
			GUIDE = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_GUIDE,
			START = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_START,
			STICK_LEFT = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_LEFTSTICK,
			STICK_RIGHT = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_RIGHTSTICK,
			LB = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_LEFTSHOULDER,
			RB = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_RIGHTSHOULDER,
			DPAD_UP = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_UP,
			DPAD_DOWN = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_DOWN,
			DPAD_LEFT = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_LEFT,
			DPAD_RIGHT = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_RIGHT,
			MAX = SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_MAX,

			/// STRETCHGOAL
			// Motions
			//STICK_LEFT_MOTION_LEFT = 16,
			//STICK_LEFT_MOTION_RIGHT = 17,
			//STICK_LEFT_MOTION_UP = 18,
			//STICK_LEFT_MOTION_DOWN = 19,
			//STICK_RIGHT_MOTION_LEFT = 20,
			//STICK_RIGHT_MOTION_RIGHT = 21,
			//STICK_RIGHT_MOTION_UP = 22,
			//STICK_RIGHT_MOTION_DOWN = 23,
			//TRIGGER_LEFT_MOTION = 24,
			//TRIGGER_RIGHT_MOTION = 25
		};

		explicit Controller();
		~Controller();

		void ProcessButton(const SDL_ControllerButtonEvent& event, const std::vector<GameObject*>& controlledGameObjects) const;
		void MapCommand(const ControllerButtonData& button, Command* pCommand);
		//void ProcessMotion(const SDL_Event& e) const;

		void OpenController(int controllerID);
		void CloseController() const;

	private:
		int m_SDLControllerID;
		SDL_GameController* m_pSDLGameController;
		std::map<ControllerButtonData, Command*> m_InputMap;
		std::vector<GameObject*> m_ControlledGameObjects;

		// Disable copy & move constructor/assignment operator
		Controller(const Controller&) = delete;
		Controller(Controller&&) noexcept = delete;
		Controller& operator=(const Controller&) = delete;
		Controller& operator=(Controller&&) noexcept = delete;
	};

	// Button helper struct
	struct ControllerButtonData
	{
		ButtonState State;
		ButtonType Type;
		Controller::Button Button;
		int ControllerID;

		ControllerButtonData()
			: State(ButtonState::NONE), Type(ButtonType::DOWN), Button(Controller::Button::MAX), ControllerID(0) {}
		ControllerButtonData(const ButtonState& state, const ButtonType& type, const Controller::Button& button, int controllerID)
			: State(state), Type(type), Button(button), ControllerID(controllerID) {}

		bool operator==(const ControllerButtonData& other)
		{
			return (State == other.State
				&& Type == other.Type
				&& Button == other.Button
				&& ControllerID == other.ControllerID);
		}
	};
	inline bool operator<(const ControllerButtonData& b1, const ControllerButtonData& b2) // used in std::map
	{
		return (b1.State < b2.State
			&& b1.Type < b2.Type
			&& b1.Button < b2.Button
			&& b1.ControllerID < b2.ControllerID);
	}
}