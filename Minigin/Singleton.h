#pragma once

namespace rde
{
	template <typename T>
	class Singleton
	{
	public:
		static T* GetInstance()
		{
			if (!m_pInstance)
				m_pInstance = new T();
			return m_pInstance;
		}
		static void DeleteInstance()
		{
			if (m_pInstance)
				delete m_pInstance;
			m_pInstance = nullptr;
		}

	protected:
		Singleton() = default;

	private:
		static T* m_pInstance;

		// Disable copy & move constructor/assignment operator
		Singleton(const Singleton&) = delete;
		Singleton(Singleton&&) noexcept = delete;
		Singleton& operator=(const Singleton&) = delete;
		Singleton& operator=(Singleton&&) noexcept = delete;
	};

	template <typename T>
	T* Singleton<T>::m_pInstance{ nullptr };
}