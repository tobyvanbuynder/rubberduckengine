#pragma once
#include "Singleton.h"

#include "Controller.h"
#include "Keyboard.h"

namespace rde
{
	class InputManager final : public Singleton<InputManager>
	{
	public:
		enum PlayerID : UINT { Player1 = 0, Player2, Player3, Player4, MaxPlayers };

		explicit InputManager();
		virtual ~InputManager() = default;

		void OnControllerOpen(int index);
		void OnControllerClose(int index);

		void ProcessController(const SDL_Event& e);
		void ProcessKeyboard(const SDL_Event& e);

		void AddControlledObjectToPlayer(UINT playerID, GameObject* pGameObj);
		void RemoveControlledObjectFromPlayer(UINT playerID, GameObject* pGameObj);

		void MapControllerButton(const ControllerButtonData& buttonData, Command* pCommand);
		void MapKeyboardButton(const KeyboardButtonData& keyboardData, Command* pCommand);

	private:
		static const UINT m_MaxConnectedControllers = 4;
		UINT m_NumConnectedControllers;
		Controller m_Controllers[m_MaxConnectedControllers];
		Keyboard m_Keyboard;
		std::vector<GameObject*> m_ControlledGameObjects[PlayerID::MaxPlayers];


		// Disable copy & move constructor/assignment operator
		InputManager(const InputManager&) = delete;
		InputManager(InputManager&&) noexcept = delete;
		InputManager& operator=(const InputManager&) = delete;
		InputManager& operator=(InputManager&&) noexcept = delete;
	};
}