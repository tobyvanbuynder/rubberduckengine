#include "MiniginPCH.h"
#include "InputManager.h"

rde::InputManager::InputManager()
	: m_Controllers{}
	, m_NumConnectedControllers(0)
	, m_ControlledGameObjects{}
	, m_Keyboard{}
{
}

void rde::InputManager::OnControllerOpen(int index)
{
	if (index + 1 > m_MaxConnectedControllers)
	{
		std::string logMsg{ "Can't connect more than " };
		logMsg.append(std::to_string(m_MaxConnectedControllers));
		logMsg.append(" Controllers!");
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Error);
		return;
	}

	m_Controllers[index].OpenController(index);
	++m_NumConnectedControllers;
}
void rde::InputManager::OnControllerClose(int index)
{
	if (index + 1 > m_MaxConnectedControllers)
	{
		Logger::GetInstance()->Log("Disconnected a controller that was not opened/supported!", Logger::LogType::Warning);
		return;
	}

	m_Controllers[index].CloseController();
	--m_NumConnectedControllers;
}


void rde::InputManager::ProcessController(const SDL_Event& e)
{
	int controllerIndex{ e.cdevice.which };
	m_Controllers[controllerIndex].ProcessButton(e.cbutton, m_ControlledGameObjects[controllerIndex]);
}

void rde::InputManager::ProcessKeyboard(const SDL_Event& e)
{
	m_Keyboard.ProcessButton(e.key.keysym.sym);
}

void rde::InputManager::AddControlledObjectToPlayer(UINT playerID, GameObject* pGameObj)
{
	/// TODO: change this
	if (playerID > m_NumConnectedControllers)
	{
		std::string logMsg{ "Controller " };
		logMsg.append(std::to_string(playerID));
		logMsg.append(" is not connected! Tried to add a controlled GameObject to InputManager");
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Warning);
		return;
	}

	auto findIt = std::find(m_ControlledGameObjects[playerID].begin(), m_ControlledGameObjects[playerID].end(), pGameObj);
	if (findIt != m_ControlledGameObjects[playerID].end())
	{
		std::string logMsg{ "Controlled GameObject already found for PlayerID " };
		logMsg.append(std::to_string(playerID));
		logMsg.append("! Trying to add duplicate GameObject to Player's GameObject list in InputManager");
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Warning);
		return;
	}
	m_ControlledGameObjects[playerID].push_back(pGameObj);
}

void rde::InputManager::RemoveControlledObjectFromPlayer(UINT playerID, GameObject* pGameObj)
{
	/// TODO: change this
	if (playerID > m_NumConnectedControllers)
	{
		std::string logMsg{ "Controller " };
		logMsg.append(std::to_string(playerID));
		logMsg.append(" is not connected! Tried to remove a controlled GameObject in InputManager");
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Warning);
		return;
	}

	auto removeIt = std::remove(m_ControlledGameObjects[playerID].begin(), m_ControlledGameObjects[playerID].end(), pGameObj);
	if (removeIt == m_ControlledGameObjects[playerID].end())
	{
		std::string logMsg{ "Controlled GameObject not found for PlayerID " };
		logMsg.append(std::to_string(playerID));
		logMsg.append("! Trying to remove GameObject from Player's GameObject list in InputManager");
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Warning);
		return;
	}
	m_ControlledGameObjects[playerID].erase(removeIt);
}

void rde::InputManager::MapControllerButton(const ControllerButtonData& buttonData, Command* pCommand)
{
	int playerID{ buttonData.ControllerID };

	if (playerID > m_MaxConnectedControllers)
	{
		std::string logMsg{ "Invalid controllerID " };
		logMsg.append(std::to_string(playerID));
		logMsg.append(". Tried to map a button to a Controller");
		Logger::GetInstance()->Log(logMsg.c_str(), Logger::LogType::Warning);
		return;
	}

	m_Controllers[playerID].MapCommand(buttonData, pCommand);
}

void rde::InputManager::MapKeyboardButton(const KeyboardButtonData& keyboardData, Command* pCommand)
{
	m_Keyboard.MapCommand(keyboardData, pCommand);
}