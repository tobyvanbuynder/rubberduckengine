#pragma once

namespace rde
{
	class Component;
	class TransformComponent;

	class GameObject final
	{
	public:
		explicit GameObject();
		virtual ~GameObject();

		void Initialize();
		void Update(float deltaTime);

		void Load() const;
		void Unload() const;

		void AddComponent(Component* pComp);

		TransformComponent* GetTransform() const;

		template<typename T>
		T* GetComponent() const;

	private:
		std::vector<Component*> m_Components;
		TransformComponent* m_pTransformComponent;
		bool m_IsInitialized;

		// Disable copy & move constructor/assignment operator
		GameObject(const GameObject&) = delete;
		GameObject(GameObject&&) noexcept = delete;
		GameObject& operator=(const GameObject&) = delete;
		GameObject& operator=(GameObject&&) noexcept = delete;
	};


	template<typename T>
	T* GameObject::GetComponent() const
	{
		// Check all Components until it's found one
		for (Component* pComp : m_Components)
		{
			// Is it this type of Component?
			if (dynamic_cast<T*>(pComp))
			{
				// Return if it is
				return static_cast<T*>(pComp);
			}
		}

		// No such Component found
		return nullptr;
	}
}