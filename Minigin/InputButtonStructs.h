#pragma once

namespace rde
{
	enum class ButtonState
	{
		PRESSED,
		RELEASED,
		NONE
	};
	enum class ButtonType
	{
		DOWN,
		UP
	};
}