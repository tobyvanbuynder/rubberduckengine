#pragma once

#include "Singleton.h"

namespace rde
{
	class FontLoader final : public Singleton<FontLoader>
	{
		typedef std::map<std::string, std::map<int, TTF_Font*>> FontMap;

	public:
		explicit FontLoader() = default;
		virtual ~FontLoader();

		TTF_Font* LoadFont(const std::string& pathToFont, int fontSize);

	private:
		FontMap m_LoadedFonts;
	};
}