#include "MiniginPCH.h"
#include "TextComponent.h"

#include "Renderer.h"
#include "GameObject.h"
#include "TransformComponent.h"
#include "TextureComponent.h"
#include "FontLoader.h"

rde::TextComponent::TextComponent(const std::string& filePath, int size, const std::string& text, const SDL_Color& color)
	: Component()
	, m_pFont{ nullptr }
	, m_pTexture{ nullptr }
	, m_FilePath{ filePath }
	, m_Size{ size }
	, m_Text{ text }
	, m_Color{ color }
	, m_NeedsUpdate{ true }
	, m_DestRect{ }
	, m_SrcRect{ }
{
}

rde::TextComponent::~TextComponent()
{
	SDL_DestroyTexture(m_pTexture);
}


void rde::TextComponent::Initialize()
{
	// Load the font
	m_pFont = FontLoader::GetInstance()->LoadFont(m_FilePath, m_Size);
}

void rde::TextComponent::Update(float)
{
	// Rendering inspired by:
	// http://gigi.nullneuron.net/gigilabs/displaying-text-in-sdl2-with-sdl_ttf/


	// Grab position from GameObject for now
	glm::vec2 pos = GetGameObject()->GetTransform()->GetPosition();

	if (m_NeedsUpdate)
	{
		// Create text
		SDL_Surface* pTextSurface = TTF_RenderText_Blended(m_pFont, m_Text.c_str(), m_Color);
		SDL_DestroyTexture(m_pTexture);
		m_pTexture = Renderer::GetInstance()->CreateTextureFromSurface(pTextSurface);

		// Clean up created resources
		SDL_FreeSurface(pTextSurface);

		m_NeedsUpdate = false;
	}

	// Create rects
	SDL_QueryTexture(m_pTexture, nullptr, nullptr, &m_DestRect.w, &m_DestRect.h); // load width and height from rendered text texture
	m_DestRect.x = int(pos.x);
	m_DestRect.y = int(pos.y);
	m_SrcRect = { 0, 0, m_DestRect.w, m_DestRect.h };

	// Add to render queue
	Renderer::GetInstance()->QueueTexture(m_pTexture, m_DestRect, m_SrcRect);
}


void rde::TextComponent::SetText(const std::string& text)
{
	m_Text = text;
	m_NeedsUpdate = true;
}

void rde::TextComponent::SetColor(const SDL_Color& color)
{
	m_Color = color;
	m_NeedsUpdate = true;
}