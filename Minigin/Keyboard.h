#pragma once

#include <map>
#include "InputButtonStructs.h"

namespace rde
{
	class Command;
	struct KeyboardButtonData;

	class Keyboard final
	{
	public:
		explicit Keyboard() = default;
		~Keyboard() = default;

		void ProcessButton(const SDL_Keycode& key);
		void MapCommand(const KeyboardButtonData& keyboardData, Command* pCommand);

	private:
		std::map<KeyboardButtonData, Command*> m_InputMap;

		// Disable copy & move constructor/assignment operator
		Keyboard(const Keyboard&) = delete;
		Keyboard(Keyboard&&) noexcept = delete;
		Keyboard& operator=(const Keyboard&) = delete;
		Keyboard& operator=(Keyboard&&) noexcept = delete;
	};

	// Button helper struct
	struct KeyboardButtonData
	{
		ButtonState State;
		ButtonType Type;
		SDL_Keycode Key;

		KeyboardButtonData()
			: State(ButtonState::NONE), Type(ButtonType::DOWN), Key(SDLK_a) {}
		KeyboardButtonData(const ButtonState& state, const ButtonType& type, const SDL_Keycode& key)
			: State(state), Type(type), Key(key) {}

		bool operator==(const KeyboardButtonData& other)
		{
			return (State == other.State
				&& Type == other.Type
				&& Key == other.Key);
		}
	};
	inline bool operator<(const KeyboardButtonData& b1, const KeyboardButtonData& b2)
	{
		return (b1.State < b2.State
			&& b1.Type < b2.Type
			&& b1.Key < b2.Key);
	}
}