#pragma once

namespace rde
{
	class Game abstract
	{
	public:
		explicit Game() = default;
		~Game() = default;

		virtual void Initialize() = 0;

	private:
		// Disable copy & move constructor/assignment operator
		Game(const Game&) = delete;
		Game(Game&&) noexcept = delete;
		Game& operator=(const Game&) = delete;
		Game& operator=(Game&&) noexcept = delete;
	};
}