#include "MiniginPCH.h"
#include "GameScene.h"

#include "GameObject.h"


rde::GameScene::GameScene(const std::string& name)
	: m_Name{ name }
	, m_IsInitialized{ false }
{
}

rde::GameScene::~GameScene()
{
	for (GameObject* pObj : m_GameObjects)
		delete pObj;
}


void rde::GameScene::Initialize()
{
	if (m_IsInitialized)
		return;

	for (size_t i{}; i < m_GameObjects.size(); ++i)
		m_GameObjects[i]->Initialize();

	m_IsInitialized = true;
}

void rde::GameScene::Update(float deltaTime)
{
	for (size_t i{}; i < m_GameObjects.size(); ++i)
		m_GameObjects[i]->Update(deltaTime);
}

void rde::GameScene::AddObject(GameObject* pObj)
{
	m_GameObjects.push_back(pObj);
}

void rde::GameScene::Load() const
{
	for (size_t i{}; i < m_GameObjects.size(); ++i)
		m_GameObjects[i]->Load();
}
void rde::GameScene::Unload() const
{
	for (size_t i{}; i < m_GameObjects.size(); ++i)
		m_GameObjects[i]->Unload();
}

std::string rde::GameScene::GetName() const
{
	return m_Name;
}