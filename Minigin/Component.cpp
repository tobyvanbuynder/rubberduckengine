#include "MiniginPCH.h"
#include "Component.h"


rde::Component::Component()
	: m_pOwningObject{ nullptr }
{
}

rde::Component::~Component()
{
}


rde::GameObject* rde::Component::GetGameObject() const
{
	return m_pOwningObject;
}