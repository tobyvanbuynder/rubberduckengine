#include "MiniginPCH.h"
#include "SceneManager.h"

#include "GameScene.h"


rde::SceneManager::SceneManager()
	: m_pActiveScene{ nullptr }
	, m_pPrevScene{ nullptr }
	, m_Scenes{ }
{
	m_Scenes.reserve(8);
}

rde::SceneManager::~SceneManager()
{
	for (GameScene* pScene : m_Scenes)
	{
		delete pScene;
	}
}


void rde::SceneManager::Initialize()
{
	// If no active scene exists
	if (m_pActiveScene == nullptr)
	{
		// If there are no scenes
		if (m_Scenes.size() == 0)
		{
			// Create resilient scene
			AddScene(new GameScene("EmptyScene"));
			m_pActiveScene = GetSceneFromName("EmptyScene");
		}
		else
		{
			// Take first scene as active scene
			m_pActiveScene = GetSceneFromName(m_Scenes[0]->GetName());
		}
	}

	// Initialize the scene if not initialized yet
	m_pActiveScene->Initialize();

	// Load the scene
	m_pActiveScene->Load();
}

void rde::SceneManager::Update(float deltaTime)
{
	m_pActiveScene->Update(deltaTime);
}

void rde::SceneManager::AddScene(GameScene* pScene)
{
	m_Scenes.push_back(pScene);
}

void rde::SceneManager::SetActiveScene(const std::string& name)
{
	m_pPrevScene = m_pActiveScene;
	m_pActiveScene = GetSceneFromName(name);

	// Create new SDL_Event
	SDL_Event sceneSwitchEvent{};
	sceneSwitchEvent.type = RDE_SCENESWITCHEVENT;
	SDL_PushEvent(&sceneSwitchEvent);
}

rde::GameScene* rde::SceneManager::GetSceneFromName(const std::string& name) const
{
	for (GameScene* pScene : m_Scenes)
	{
		if (pScene->GetName() == name)
			return pScene;
	}

	return nullptr;
}

void rde::SceneManager::SwitchScene()
{
	if (m_pPrevScene != nullptr)
		m_pPrevScene->Unload();

	m_pActiveScene->Initialize();
	m_pActiveScene->Load();
	m_pPrevScene = nullptr;
}