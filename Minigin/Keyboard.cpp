#include "MiniginPCH.h"
#include "Keyboard.h"

void rde::Keyboard::ProcessButton(const SDL_Keycode& key)
{
	Logger::GetInstance()->Log(SDL_GetKeyName(key));
}

void rde::Keyboard::MapCommand(const KeyboardButtonData& keyboardData, Command* pCommand)
{
	m_InputMap[keyboardData] = pCommand;
}